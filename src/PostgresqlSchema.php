<?php 

namespace PostgresqlSchema;


class UpdateResult
{
  public $id = -2;
  public $msg = "";
  public function __construct($_id,$_msg) {
    $this->id = $_id;
    $this->msg = $_msg;
  }
}

class SchemaManager
{

    protected $db_host = Null;

    protected $db_user = Null;

    protected $db_pass = Null;

    protected $db_name = Null;

    protected $pdo = Null;
    
    public function __construct()
    {
        //parent::__construct();
        $this->db_host = $this->get_cfg_from_env('DATABASE_SERVICE_NAME','localhost');
        $this->db_user = $this->get_cfg_from_env('POSTGRESQL_USER','app');
        $this->db_pass = $this->get_cfg_from_env('POSTGRESQL_PASSWORD','app');
        $this->db_name = $this->get_cfg_from_env('POSTGRESQL_DATABASE','app');
    }

    public function init() {
        $this->connect(); 
        $this->createSchemaTables();
    }

    private function get_cfg_from_env($name, $default='') {
        $ret = $default;
        $tmp = getenv($name);
        if( isset($tmp) and $tmp != '' ) {
            $ret = $tmp;
        }
        return $ret;
    }

    private function connect() {

        // connect to the postgresql database
        $conStr = sprintf("pgsql:host=%s;dbname=%s;user=%s;password=%s", 
                $this->db_host, 
                $this->db_name, 
                $this->db_user, 
                $this->db_pass);
 
        $this->pdo = new \PDO($conStr);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    protected function createSchemaTables() {
        $sqlList = ['CREATE TABLE IF NOT EXISTS schema_version (
                        id INTEGER PRIMARY KEY,
                        created date NOT NULL,
                        description character varying(255) NOT NULL
                     )',
                    ];
 
        // execute each sql statement to create new tables
        foreach ($sqlList as $sql) {
            $this->pdo->exec($sql);
        }
    }

    public function getTables() {
        $stmt = $this->pdo->query("SELECT table_name 
                                   FROM information_schema.tables 
                                   WHERE table_schema= 'public' 
                                        AND table_type='BASE TABLE'
                                   ORDER BY table_name");
        $tableList = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $tableList[] = $row['table_name'];
        }
 
        return $tableList;
    }

    private function get_schema_version() {
        $sql = "SELECT MAX(id) AS max_id FROM schema_version";
        $stmt = $this->pdo->query($sql);
        $max = 0;
        while( $row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $max = (int)$row['max_id'];
        }
        return $max;
    }

    private function insertUpdate($id, $description) {
        $sql = "INSERT INTO schema_version (id,description,created) VALUES (:id,:description,NOW())";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(":id", $id);
        $stmt->bindValue(":description", $description);

        $stmt->execute();
    }

    public function update_next() {

        $max_id = $this->get_schema_version();
        $ret = Null;
        switch($max_id) {
          case 0:
            $ret = $this->update1();
            break;
          case 1:
            $ret = $this->update2();
            break;
          default:
            $ret = new UpdateResult(-1,"no updates required");
        }

        return $ret;
    }

    private function update1() {
        $_id = 1;
        $_description = "Table ticker created";

        $sqlList = ['CREATE TABLE IF NOT EXISTS ticker (
                        id SERIAL PRIMARY KEY,
                        created date default CURRENT_DATE,
                        message character varying(255) NOT NULL
                     )',
                    ];
 
        // execute each sql statement to create new tables
        foreach ($sqlList as $sql) {
            $this->pdo->exec($sql);
        }
        $this->insertUpdate($_id,$_description);
        return new UpdateResult($_id,$_description);
    }

    private function update2() {
        $_id = 2;
        $_description = "Column ticker created convered to timestamp";

        $sqlList = ['ALTER TABLE ticker ALTER COLUMN created SET DATA TYPE timestamp',
                    'ALTER TABLE ticker ALTER COLUMN created SET DEFAULT CURRENT_TIMESTAMP'];
 
        // execute each sql statement to create new tables
        foreach ($sqlList as $sql) {
            $this->pdo->exec($sql);
        }
        $this->insertUpdate($_id,$_description);
        return new UpdateResult($_id,$_description);
    }

    public function getTicker() {
        $ret = array();
        $sql = "SELECT id,TO_CHAR( created, 'DD.MM.YYYY HH24:MI:SS') AS created,message FROM ticker ORDER BY id DESC LIMIT 10";
        $stmt = $this->pdo->query($sql);
        while( $row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
          $ret[] = $row;
        }
        return $ret;
    }

    public function addTickerMessage($msg) {
        $sql = "INSERT INTO ticker (message) VALUES (:message)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(":message", $msg);

        $stmt->execute();

    }
}
