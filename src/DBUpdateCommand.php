<?php

namespace Console;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Console\Command;




/**
 * Author: 
 */
class DBUpdateCommand extends DBUpdate
{
   
    public function configure()
    {

        $this -> setName('dbupdate')
            -> setDescription('Update the database schema')
            -> setHelp('This command will update your DB-Schema to the current version');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $this -> update($input, $output);
    }
}
?>
