<?php 

namespace Console;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
/**
 * Author: Chidume Nnamdi <kurtwanger40@gmail.com>
 */
class DBUpdate extends SymfonyCommand
{

    protected $db_host = Null;

    protected $db_user = Null;

    protected $db_pass = Null;

    protected $db_name = Null;

    protected $pdo = Null;

    protected $schemaManager = Null;
    
    public function __construct($sm)
    {
        parent::__construct();
        $this->schemaManager = $sm;
    }

    protected function update(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output -> writeln([
            '====**** DBUpdate Console App ****====',
            '======================================',
            '',
        ]);
        
        // outputs a message without adding a "\n" at the end of the line
        $output -> writeln("Updating");

        $output -> writeln( $this->db_host );
        $output -> writeln( $this->db_user );
        $output -> writeln( $this->db_pass );
        $output -> writeln( $this->db_name );

        $this->schemaManager->init();
        $tables = $this->schemaManager->getTables();
        foreach( $tables as $table ) {
          $output->writeln($table);
        }

        while( ($res = $this->schemaManager->update_next())->id >= 0 ) {
            $output->writeln(print_r($res,True));
            $output->writeln($res->msg);
        }

    }

}
