<?php 

namespace Console;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
/**
 * Author: 
 */
class TickerCronNew extends SymfonyCommand
{
    protected $schemaManager = Null;

    private $messages = array(
      'Moin moin','Hello World','Tach auch','Hallo'
    );
    
    public function __construct($sm)
    {
        parent::__construct();
        $this->schemaManager = $sm;
    }
    protected function generateTickerMessage(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output -> writeln([
            '====**** DBUpdate Console App ****====',
            '======================================',
            '',
        ]);

        $this->schemaManager->init();
        
        $new_message = $this->getRandomTicketMessage();
        $this->schemaManager->addTickerMessage($new_message);

        // outputs a message without adding a "\n" at the end of the line
        $output -> writeln("The new message was inserted:" . $new_message);
    }

    private function getRandomTicketMessage()
    {
        $rk = array_rand($this->messages, 1);
        return $this->messages[$rk];
    }
}
