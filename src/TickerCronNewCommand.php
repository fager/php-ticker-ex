<?php

namespace Console;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Console\TickerCronNew;
/**
 * Author: 
 */
class TickerCronNewCommand extends TickerCronNew
{
    
    public function configure()
    {
        $this -> setName('newmessage')
            -> setDescription('add a new ticker message')
            -> setHelp('This command allows you to save a new ticker message in the database');
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this -> generateTickerMessage($input, $output);
    }
}
?>
