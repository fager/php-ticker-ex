#!/usr/bin/env php
<?php
/** console.php **/

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Console\TimeCommand;
use Console\DBUpdateCommand;
use Console\TickerCronNewCommand;
use PostgresqlSchema\SchemaManager as SchemaManager;

$sm = new SchemaManager();

$app = new Application();
$app -> add(new TimeCommand());
$app -> add(new DBUpdateCommand($sm));
$app -> add(new TickerCronNewCommand($sm));
$app -> run();

