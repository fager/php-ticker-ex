<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ticker Example</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </head>
  <body>

<?php
 
require 'vendor/autoload.php';
 
use PostgresqlSchema\SchemaManager as SchemaManager;
 
try {
    
    // connect to the PostgreSQL database
    $sm = new SchemaManager();
    $sm->init();

    $tables = $sm->getTables();

    echo "Tabellen:<br>\n";
    echo "<ul>\n";
    foreach ($tables as $table){
        echo "<li>" . $table . "</li>\n";
    }
    echo "</ul>\n";
    echo "<br><br>\n";

    $ticket = $sm->getTicker();
    echo "Ticker:<br>\n";
    echo "<ul>\n";
    foreach ($ticket as $tic){
        echo "<li>" . $tic['message'] . " (". $tic['created'].")</li>\n";
    }
    echo "</ul>\n";

    echo "<br><br>\n";
    echo "Software Version: " . getenv("OPENSHIFT_BUILD_COMMIT");

    

    
} catch (\PDOException $e) {
    echo $e->getMessage();
}
?>
  </body>
</html>
